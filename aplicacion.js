class Persona{
    constructor(nombre,edad,fecha){
        this.nombre = nombre,
        this.edad = edad,
        this.fecha = fecha
    }
}

class Interface{

    listaPersonal(objPersona){
        
        let contenedorPersona = document.getElementById("listadoUsuario");
        let contenedor = document.createElement("div");

        contenedor.innerHTML = `
            <div class="pb-3">
                <div class="card text-center">
                    <div class="card-body">
                        <strong>Nombre: </strong> ${objPersona.nombre}
                        <strong>Edad: </strong> ${objPersona.edad}
                        <strong>Fecha: </strong> ${objPersona.fecha}
                        <button class="btn btn-danger" id="Eliminar" name="delete">Eliminar</button>
                    </div>
                </div>
            <div>
        `;

        contenedorPersona.appendChild(contenedor);        

    }

    limpiarFormulario(){
        document.getElementById("NuevoPersonal").reset();
    }

    eliminarCard(valor){

        if (valor.name == "delete") {
            valor.parentElement.parentElement.parentElement.remove();   
        }

    }

}

// DOM DEL NAVEGADOR

document.getElementById("NuevoPersonal").addEventListener('submit', function (e) {
    
    e.preventDefault();

    let nombre = document.getElementById("nombre").value;
    let edad = document.getElementById("edad").value;
    let fecha = document.getElementById("fechaRegistro").value;
    
    const newPersona = new Persona(nombre,edad,fecha);
    const interfacePersona = new Interface();
    interfacePersona.listaPersonal(newPersona);
    interfacePersona.limpiarFormulario();

})

document.getElementById("listadoUsuario").addEventListener('click', function (e) {
    
    let eliminar = e.target;
    const interfacePersona = new Interface();
    interfacePersona.eliminarCard(eliminar);

})